function objAdd = extract_object(img, map, objPath, objInd)

    val_th = 255; % pixel brightness threshold
    size_th = 32; % object size
    density_th = 0.5;

    th_img = (map >= val_th);

    xsum = find(sum(th_img, 1));
    
    objAdd = 0;
    if isempty(xsum)
        return;
    end

    stats = regionprops(th_img, 'BoundingBox', 'Area');
    
    for i = 1:size(stats, 1)
        width = stats(i,:).BoundingBox(3);
        height = stats(i,:).BoundingBox(4);
        if (height >= size_th && width >= size_th) 
            xmin = floor(stats(i,:).BoundingBox(1));
            ymin = floor(stats(i,:).BoundingBox(2));
            xmax = ceil(stats(i,:).BoundingBox(1)+width);
            ymax = ceil(stats(i,:).BoundingBox(2)+height);
            density = stats(i,:).Area / (width*height);
            
            if density > density_th
                object = img(ymin:ymax, xmin:xmax, :);
                alpha = map(ymin:ymax, xmin:xmax);
                imwrite(object, [objPath, 'object_',sprintf('%04d',objInd+objAdd), '.png'], 'Alpha', alpha);
                objAdd = objAdd + 1;
            end
        end
    end
    
end
