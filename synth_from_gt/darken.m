function [RGB_darkened] = darken(XYZ, RGB)
    dist = zeros(1, size(XYZ(3,:),2));
    dist(1,:) = sqrt(XYZ(1,:).*XYZ(1,:) + XYZ(2,:).*XYZ(2,:) + XYZ(3,:).*XYZ(3,:));
    distsq(1,:) = 0.3*dist(1,:).*dist(1,:);
    RGB_darkened(1,:) = uint8(double(RGB(1,:))./distsq(1,:));
    RGB_darkened(2,:) = uint8(double(RGB(2,:))./distsq(1,:));
    RGB_darkened(3,:) = uint8(double(RGB(3,:))./distsq(1,:));
end

