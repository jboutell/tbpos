% Synthesize query images from given poses
% authors:
% Masud Fahim, Jani Boutellier
% University of Vaasa, 2022

addpath('../common') 

%% provide desired query image size
hw = 1024;
hh = 768;

%% set camera intrinsics
f = 886;                         % focal length given insetup
K = [f, 0, hw/2; 0, f, hh/2; 0, 0, 1];  % set camera intr. matrix

image_ind = 1;
dark = 1;
objects = 0;
occlusion = 50;

files = dir(fullfile('image/', '*.png'));
files = files(~[files.isdir]);
nFiles = size(files,1);

for i = 1:nFiles
    imageFile = files(i).name;
    posFile = strrep(imageFile,'.png','.mat');
    scanFile = strrep(imageFile,'.png','.txt');
    fid = fopen(['scan_idx/',scanFile]);
    scanIndTxt = fgetl(fid);
    scanInd = str2double(scanIndTxt);
    fclose(fid);
    pcl = load(['../database/scans/',sprintf('%03d',scanInd),'/TB_scan_', sprintf('%03d',scanInd), '.ptx.mat']);
    A = pcl.A;
    [~, Rt] = load_WUSTL_transformation(['../database/alignments/',sprintf('%03d',scanInd),'/transformations/TB_trans_', sprintf('%03d',scanInd), '.txt']);
    pos_s = load(['gt/', posFile]);
    world_P = pos_s(1).pos;

    local_P = world_P*Rt;
    rot_xyz = anglesR(local_P(1:3,1:3),'xyz');

    clear XYZ;
    clear RGB;
    XYZ = [A{1}, A{2}, A{3}]';
    RGB = [A{5}, A{6}, A{7}]';
    
    if dark == 1
        RGB = darken(XYZ, RGB);
    end

    % Transform point cloud from local to world coordinates
    XYZ = Rt * [XYZ; ones(1, length(XYZ))];
    XYZ = bsxfun(@rdivide, XYZ(1:3, :), XYZ(4, :));

    if objects == 1
        object_count = 4;
        object_angle = rot_xyz(3);
        object_trans = [0 0 0];
        [XYZ_obj, RGB_obj] = add_n_objects(object_trans, object_angle, ceil(rand(1,1)*object_count));
        RGB_obj = darken(XYZ_obj, RGB_obj);
        XYZ_obj = Rt * [XYZ_obj; ones(1, length(XYZ_obj))];
        XYZ_obj = bsxfun(@rdivide, XYZ_obj(1:3, :), XYZ_obj(4, :));
        [view_obj, ~, dist_obj] = ht_Points2Persp(RGB_obj, XYZ_obj, K*world_P, hh, hw); 
    end

    [view_bg, ~, dist_bg] = ht_Points2Persp(RGB, XYZ, K*world_P, hh, hw); 

    Ind = sprintf('%03d', image_ind);
    gtName = ['gt-out', '/IMG_', Ind, '.mat'];
    pos = pos_s(1).pos;
    save(gtName, 'pos');

    scName = ['scan-out', '/IMG_', Ind, '.txt'];
    fid = fopen(scName, 'w');
    fprintf(fid, '%i', scanInd);
    fclose(fid);

    imgName = ['query', '/IMG_', Ind, '.png'];
    fprintf('%s: ', imgName);
    view_filled = gapfill(view_bg);
    if objects == 1
        view_final = combine(view_filled, dist_bg, view_obj, dist_obj);
    else
        view_final = view_filled;
    end
    if occlusion > 0
        view_final = occlude(view_final, occlusion);
    end
    image_ind = image_ind + 1;
    imwrite(view_final, imgName);

end

