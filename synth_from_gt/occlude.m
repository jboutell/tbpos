function occluded = occlude(original, occlusion)

    [H, W, ~] = size(original);
    area = W*H;
    while 100*area/(W*H) > occlusion
        x1 = round(rand(1,1)*W)-(W/4); % top-left
        x1 = max(1, x1);
        x2 = x1 + round(rand(1,1)*(0.75*W)); % top-right
        x2 = min(W, x2);
        x3 = x2 + round((rand(1,1)-0.5)*occlusion*3); %bottom-right
        x3 = min(W, x3);
        x4 = x3 - round(rand(1,1)*W); %bottom-left
        x4 = max(1, x4);
        area = polyarea([x1 x2 x3 x4], [1 1 H H]);
    end
    pos_hexagon = [x1 1 x2 1 x3 H x4 H];
    occluded = insertShape(original, 'FilledPolygon', pos_hexagon, ...
        'Color', {'black'}, 'Opacity', 1);
    fprintf('%i%% occluded\n', round(100*area/(W*H)));
end

