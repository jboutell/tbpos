function view = combine(bg_rgb, bg_dist, obj_rgb, obj_dist)
    [H, W] = size(bg_dist);
    view = zeros(H, W, 3);
    
    bg_nan = isnan(bg_dist);
    bg_dist(bg_nan) = 10000;
    
    objpix = 0;
    bgpix = 0;
    
    for h = 1:H
        for w = 1:W
            if isnan(obj_dist(h, w))
                view(h, w, 1) = bg_rgb(h, w, 1); 
                view(h, w, 2) = bg_rgb(h, w, 2); 
                view(h, w, 3) = bg_rgb(h, w, 3);
                bgpix = bgpix + 1;
            else    
                if obj_dist(h, w) <= bg_dist(h, w)
                    view(h, w, 1) = obj_rgb(h, w, 1); 
                    view(h, w, 2) = obj_rgb(h, w, 2); 
                    view(h, w, 3) = obj_rgb(h, w, 3);
                    objpix = objpix + 1;
                else
                    view(h, w, 1) = bg_rgb(h, w, 1); 
                    view(h, w, 2) = bg_rgb(h, w, 2); 
                    view(h, w, 3) = bg_rgb(h, w, 3);
                    bgpix = bgpix + 1;
                end
            end
        end
    end
    
    view = uint8(view);
end

