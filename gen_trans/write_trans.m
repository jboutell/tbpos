function write_trans(name, T)

    T = T';
    fid = fopen(name, 'w');

    fprintf(fid, 'Before general icp:\n');
    fprintf(fid, ' %.6f  %.6f  %.6f  %.6f\n', T(1,1), T(1,2), T(1,3), T(1,4));
    fprintf(fid, ' %.6f  %.6f  %.6f  %.6f\n', T(2,1), T(2,2), T(2,3), T(2,4));
    fprintf(fid, ' %.6f  %.6f  %.6f  %.6f\n', T(3,1), T(3,2), T(3,3), T(3,4));
    fprintf(fid, '        0         0         0         1\n');
    fprintf(fid, '\n');
    fprintf(fid, 'After general icp:\n');
    fprintf(fid, ' %.6f  %.6f  %.6f  %.6f\n', T(1,1), T(1,2), T(1,3), T(1,4));
    fprintf(fid, ' %.6f  %.6f  %.6f  %.6f\n', T(2,1), T(2,2), T(2,3), T(2,4));
    fprintf(fid, ' %.6f  %.6f  %.6f  %.6f\n', T(3,1), T(3,2), T(3,3), T(3,4));
    fprintf(fid, '        0          0        0         1\n');

    fclose(fid);

end

