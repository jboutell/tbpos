addpath('../common') 

%% Output cutout resolution 
w = 1024;
h = 768;

for ik = 1:182
    imfold = ['../cutouts/', num2str(ik-1,'%03d')];
    mkdir(imfold);
    for ii = 1:12
        A = load(['../scans/TB_scan_',sprintf('%03d',ik-1),'.ptx.mat']).A;
        XYZ = [A{1}, A{2}, A{3}]';
        RGB = [A{5}, A{6}, A{7}]';
        theta = 30 * (ii-1);

        %% Straight ahead
        [s1, s2] = view_syn_dtbs(RGB, XYZ, theta, 90, h, w);   
        s1 = uint8((gapfill(s1))); 
        imgName = [imfold,'/TB_cutout_',num2str(ik-1,'%03d'),'_',num2str(theta),'_',num2str(0), '.jpg'] ;
        imwrite(s1, imgName, "Quality", 95);
        RGBcut = s1;
        XYZcut = s2; 
        matName = [imfold,'/TB_cutout_',num2str(ik-1,'%03d'),'_',num2str(theta),'_',num2str(0), '.jpg','.mat'] ;
        save(matName,'RGBcut','XYZcut');

        %% Upwards
        [s3, s4] = view_syn_dtbs(RGB, XYZ, theta, 60, h, w);   
        s3 = uint8((gapfill(s3))); 
        imgName = [imfold,'/TB_cutout_',num2str(ik-1,'%03d'),'_',num2str(theta),'_',num2str(30), '.jpg'] ;
        imwrite(s3, imgName, "Quality", 95) ;
        RGBcut = s3;
        XYZcut = s4; 
        matName = [imfold,'/TB_cutout_',num2str(ik-1,'%03d'),'_',num2str(theta),'_',num2str(30), '.jpg','.mat'] ;
        save(matName,'RGBcut','XYZcut');

        %% Downwards
        [s5, s6] = view_syn_dtbs(RGB, XYZ, theta, 120, h, w);   
        s5 = uint8((gapfill(s5))); 
        imgName = [imfold,'/TB_cutout_',num2str(ik-1,'%03d'),'_',num2str(theta),'_',num2str(-30), '.jpg'] ;
        imwrite(s5, imgName, "Quality", 95);    
        RGBcut = s5;
        XYZcut = s6; 
        matName = [imfold,'/TB_cutout_',num2str(ik-1,'%03d'),'_',num2str(theta),'_',num2str(-30), '.jpg','.mat'] ;
        save(matName,'RGBcut','XYZcut');

    end   
    fprintf(' completed cutout %d\n', ik-1);
end
