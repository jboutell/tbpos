function [sop] = im2st(img)

    cp1 = img(:,:,1);
    v1 = cp1(:);

    cp2 = img(:,:,2);
    v2 = cp2(:);

    cp3 = img(:,:,3);
    v3 = cp3(:);

    sop = [v1, v2, v3];
end
