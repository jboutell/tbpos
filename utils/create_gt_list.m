%% Create ground truth file for evaluation
files = dir(fullfile('.', '*.png'));
nFiles = size(files,1);

for i = 1:nFiles
    DUC1_RefList(i).queryname = files(i).name;
    pngName = files(i).name;
    matName = strrep(pngName,'.png','.mat')
    load(['../gt/',matName]);
    DUC1_RefList(i).P = pos;
end

save('TBPos_refposes_all.mat', 'DUC1_RefList');
