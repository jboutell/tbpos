function [sm] = count0(img)
    blackPixels = img(:,:,1) == 0 & img(:,:,2) == 0 & img(:,:,3) == 0;
    sm = sum(blackPixels(:));
end
