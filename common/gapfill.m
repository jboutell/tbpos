function [sop] = gapfill(img)
    sop = img;
    [h, w, ~] = size(img);
    zeroCnt = count0(sop);
    rounds = 0;
    while (zeroCnt > 0)
        [row, col] = find(sop(:,:,1) == 0 & sop(:,:,2) == 0 & sop(:,:,3) == 0);
        out = sop;
        for i = 1:length(row)
            newval = nninterp(sop, row(i), col(i));
            out(row(i),col(i),:) = uint8(newval);
        end
        sop = out;
        zeroCnt = count0(sop);
        rounds = rounds + 1;
    end
end