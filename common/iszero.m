function bl = iszero(colorpixel)
    bl = 0;
    if colorpixel(1,1,1) == 0 && colorpixel(1,1,2) == 0 && colorpixel(1,1,3) == 0
        bl = 1;
    end
end

