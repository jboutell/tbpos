% Extract objects based on saliency maps
% to be used with the output of 
% https://github.com/jinglou/p2019-cns-sod
% Jing Lou et al. "Exploiting Color Name Space for Salient Object Detection," Multimedia Tools and Applications, vol. 79, no. 15
%
% code author: Jani Boutellier, University of Vaasa, Finland

%% Path
if exist('objects', 'dir') ~= 7
    system('mkdir objects');
end

imgPath = 'jpgs/';
rstPath = 'maps/';
objPath = 'objects/';

%% Read Images
imgs = dir([imgPath, '*.jpg']);

objInd = 1;

for imgno = 1:length(imgs)    
    imgname = imgs(imgno).name;
    t1 = clock;
    fprintf('%04d/%04d - %s\n', imgno, length(imgs), imgname);

    img = imread([imgPath, imgname]);
    map = imread([rstPath, imgname(1:end-4), '_CNS.png']);
    objAdd = extract_object(img, map, objPath, objInd);
    objInd = objInd + objAdd;

    t2 = clock;
    fprintf('(Time: %fs)\n', etime(t2,t1));
end
