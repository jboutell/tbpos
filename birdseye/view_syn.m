function [RGBgen, x, y] = view_syn(RGB, XYZ, Kin, hh, hw, vect)

    x_shift = vect[1];
    y_shift = vect[2];
    z_shift = vect[3];
    x_rot = vect[4];
    y_rot = vect[5];
    z_rot = vect[6];
    fprintf('Rendering %d %d %d %.1f %.1f %.1f \n', x_rot, y_rot, z_rot, x_shift, y_shift, z_shift);

    % establish projection matrix in local coordinates
    P_local(1:3, 1:3) = rotx(90-x_rot) * roty(y_rot) * rotz(z_rot);
    P_local(:, 4) = [x_shift y_shift z_shift];

    [RGBgen, ~, x, y] = ppp(RGB, XYZ, Kin, P_local, hh, hw);

end
