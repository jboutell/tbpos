% Create a combined point cloud from individual point clouds
% The default setting of skip = 10 takes every 10th point
% and creates a combined point cloud of aroung 10 GB
AP_ind = 1;
skip = 10

for ik = 0:1:181
    pcl = load(['../database/scans/',sprintf('%03d',ik),'/TB_scan_', sprintf('%03d',ik), '.ptx.mat']);
    wd = pcl.A;
    fid = ['../database/alignments/',sprintf('%03d',ik),'/transformations/TB_trans_', sprintf('%03d',ik), '.txt']
    [~, Rt] = load_WUSTL_transformation(fid);

    XYZ_tmp = [wd{1}(1:skip:end), wd{2}(1:skip:end), wd{3}(1:skip:end)]';
    % Transform point cloud from local to world coordinates
    XYZ_tmp = Rt * [XYZ_tmp; ones(1, length(XYZ_tmp))];
    XYZ_tmp = bsxfun(@rdivide, XYZ_tmp(1:3, :), XYZ_tmp(4, :));

    [~, AP_inc] = size(XYZ_tmp);

    XYZ(:,AP_ind:AP_ind+AP_inc-1) = XYZ_tmp;
    RGB(:,AP_ind:AP_ind+AP_inc-1) = [wd{5}(1:skip:end), wd{6}(1:skip:end), wd{7}(1:skip:end)]';
    fprintf("Index: %.0f Start: %.0f, End: %.0f\n", ik, AP_ind, AP_ind+AP_inc-1);
    AP_ind = AP_ind + AP_inc;
end

clear pcl;
clear wd;
clear XYZ_tmp;
save ('combinedPC.mat', XYZ, RGB, '-v7.3');
