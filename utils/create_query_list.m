%% Create query image list required by InLoc
files = dir(fullfile('.', '*.png'));
nFiles = size(files,1);
query_imgnames_all = cell(1,nFiles);

for i=1:nFiles
    query_imgnames_all{1,i} = files(i).name;
end

save('query_imgnames_all.mat', 'query_imgnames_all');
