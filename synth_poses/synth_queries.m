% Synthesize query image poses
% authors:
% Masud Fahim, Jani Boutellier
% University of Vaasa, 2022

addpath('../common') 

%% provide temporary image size for gap counting
hw = 640;
hh = 480;

%% set camera intrinsics
half_fov = 30;
f = floor((hw/2)/tan(pi/180*half_fov)); % focal length given insetup
K = [f, 0, hw/2; 0, f, hh/2; 0, 0, 1];  % set camera intr. matrix

image_ind = 1;

for ik = 0:181

    pcl = load(sprintf('../database/scans/%03i/TB_scan_%03i.ptx.mat', ik, ik));
    A = pcl.A;
    [~, Rt] = load_WUSTL_transformation(sprintf('../database/alignments/%03i/transformations/TB_trans_%03i.txt', ik, ik));

    XYZ = [A{1}, A{2}, A{3}]';
    RGB = [A{5}, A{6}, A{7}]';

    % Transform point cloud from local to world coordinates
    XYZ = Rt * [XYZ; ones(1, length(XYZ))];
    XYZ = bsxfun(@rdivide, XYZ(1:3, :), XYZ(4, :));

    imgf ='query';
    imgp ='gt';
    imgsc = 'scan_idx';

    count = 0;
    count_max = 4;
    mul = 1;
    fail_count = 0;

    while count < count_max
        [view_orig, pos] = view_syn_qry(RGB, XYZ, K, hh, hw, Rt);
        ratio = zeratio(view_orig);
        if (ratio < 0.05*mul && ratio > 0.01)
            imgInd = sprintf('%03d', image_ind);
            imgName = [imgf, '/IMG_', imgInd, '.png'];
            posName = [imgp, '/IMG_', imgInd, '.mat'];
            scnName = [imgsc, '/IMG_', imgInd, '.txt'];
            fprintf(' (%.2f) writing image %s ..', ratio, imgName);
            count = count + 1;
            image_ind = image_ind + 1;
            imwrite(view_orig, imgName);
            save(posName, 'pos');
            fileID = fopen(scnName,'w');
            fprintf(fileID,'%i\n', ik);
            fclose(fileID);
            fprintf('done\n');
        else
            fprintf('.');
            fail_count = fail_count + 1;
            if fail_count > 50
                if mul == 3
                    continue;
                end
                mul = mul + 1;
                fail_count = 0;
            end
        end
    end
end
