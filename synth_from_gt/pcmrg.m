function [pot] = pcmrg(pc1, pc2)
    po1 = [pc1';pc2'];
    pot = po1';
end

