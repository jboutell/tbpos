function [finalval] = nninterp(image, y, x)
    %we assume point x,y in image is zero
    [H, W, ~] = size(image);
    colorcount = 0;
    
    newval = double(zeros(1,1,3));
    if y > 1 && x > 1
        n1 = double(image(y-1,x-1,:));
        isz = iszero(n1);
        if isz == 0 % this neighbor is not black
            newval(1,1,1) = newval(1,1,1) + n1(1,1,1);
            newval(1,1,2) = newval(1,1,2) + n1(1,1,2);
            newval(1,1,3) = newval(1,1,3) + n1(1,1,3);
            colorcount = colorcount + 1;
        end
    end
    if y > 1
        n2 = double(image(y-1,x,:));
        isz = iszero(n2);
        if isz == 0
            newval(1,1,1) = newval(1,1,1) + n2(1,1,1);
            newval(1,1,2) = newval(1,1,2) + n2(1,1,2);
            newval(1,1,3) = newval(1,1,3) + n2(1,1,3);
            colorcount = colorcount + 1;
        end
    end
    if y > 1 && x < W
        n3 = double(image(y-1,x+1,:));
        isz = iszero(n3);
        if isz == 0
            newval(1,1,1) = newval(1,1,1) + n3(1,1,1);
            newval(1,1,2) = newval(1,1,2) + n3(1,1,2);
            newval(1,1,3) = newval(1,1,3) + n3(1,1,3);
            colorcount = colorcount + 1;
        end
    end
    if x > 1
        n4 = double(image(y,x-1,:));
        isz = iszero(n4);
        if isz == 0
            newval(1,1,1) = newval(1,1,1) + n4(1,1,1);
            newval(1,1,2) = newval(1,1,2) + n4(1,1,2);
            newval(1,1,3) = newval(1,1,3) + n4(1,1,3);
            colorcount = colorcount + 1;
        end
    end
    if x < W
        n5 = double(image(y,x+1,:));
        isz = iszero(n5);
        if isz == 0
            newval(1,1,1) = newval(1,1,1) + n5(1,1,1);
            newval(1,1,2) = newval(1,1,2) + n5(1,1,2);
            newval(1,1,3) = newval(1,1,3) + n5(1,1,3);
            colorcount = colorcount + 1;
        end
    end
    if y < H && x > 1
        n6 = double(image(y+1,x-1,:));
        isz = iszero(n6);
        if isz == 0
            newval(1,1,1) = newval(1,1,1) + n6(1,1,1);
            newval(1,1,2) = newval(1,1,2) + n6(1,1,2);
            newval(1,1,3) = newval(1,1,3) + n6(1,1,3);
            colorcount = colorcount + 1;
        end
    end
    if y < H
        n7 = double(image(y+1,x,:));
        isz = iszero(n7);
        if isz == 0
            newval(1,1,1) = newval(1,1,1) + n7(1,1,1);
            newval(1,1,2) = newval(1,1,2) + n7(1,1,2);
            newval(1,1,3) = newval(1,1,3) + n7(1,1,3);
            colorcount = colorcount + 1;
        end
    end
    if y < H && x < W
        n8 = double(image(y+1,x+1,:));
        isz = iszero(n8);
        if isz == 0
            newval(1,1,1) = newval(1,1,1) + n8(1,1,1);
            newval(1,1,2) = newval(1,1,2) + n8(1,1,2);
            newval(1,1,3) = newval(1,1,3) + n8(1,1,3);
            colorcount = colorcount + 1;
        end
    end
    finalval = double(zeros(1,1,3));
    if colorcount >= 3
        finalval(1,1,1) = newval(1,1,1) / colorcount;
        finalval(1,1,2) = newval(1,1,2) / colorcount;
        finalval(1,1,3) = newval(1,1,3) / colorcount;
    end 
end

