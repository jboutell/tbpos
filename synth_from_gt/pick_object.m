function [imageUp, alphaUp] = pick_object(folder)
    imgs = dir([folder, '*.png']);
    nimg = length(imgs);
    ind = ceil(rand(1,1)*(nimg-1));
    fn = imgs(round(ind+1)).name;
    [image, ~, alpha] = imread([folder, fn]);
    imageUp = imresize(image, 3, 'nearest');
    alphaUp = imresize(alpha, 3, 'nearest');
end
