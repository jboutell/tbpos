function coords3d = project_xyz(image, distance, scale, y_offset, alpha)

    [height, width, ~] = size(image);

    coords3d = zeros(height, width, 3); % last dimension is 3d coordinates
                                        % in order y, x, z
    for i = 1:height
        for j = 1:width
            if (alpha(i, j) > 0)
                coords3d(i, j, 1) = scale*(ceil(width/2)-j);  % x coordinate
                coords3d(i, j, 3) = distance;             % z coordinate
                coords3d(i, j, 2) = scale*(i + y_offset); % y coordinate
            end
        end
    end
    
end

