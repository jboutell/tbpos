%% Create cutout list matlab file required by InLoc 
fid = fopen('filelist.txt');
tline = fgetl(fid);
cutout_imgnames_all = cell(1,6552);
i = 1;
while ischar(tline)
    cutout_imgnames_all{1,i} = tline;
    tline = fgetl(fid);
    i = i + 1;
end
save('cutout_imgnames_all.mat', 'cutout_imgnames_all');
fclose(fid);



