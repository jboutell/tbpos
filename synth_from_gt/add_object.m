function [xyz, rgb] = add_object(XYZ, RGB, angle, origin, ind)

    [kmg, alpha] = pick_object('objects/');

    [h1,w1,ch]= size(kmg);

    distance = rand(1,1)*4+1;
    scale = .004/3;
    cam_height = 1.0;
    origin(3) = 0;
    [XYZ_obj, RGB_obj] = dbitmap2pcl(kmg, distance, scale, cam_height/scale-h1, alpha, origin, [0, angle, 0]);

    %%% merging points from PCL and image
    xyz = pcmrg(XYZ, XYZ_obj);

    %% merging colors from PCL and image
    rgb = pcmrg(RGB, RGB_obj);

end

