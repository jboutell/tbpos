function [ratio] =  zeratio(img)
    zeros = count0(img);
    [rows, cols, ~] = size(img);
    totpxl = rows*cols;
    ratio = zeros / totpxl;
end

