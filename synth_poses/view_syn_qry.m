function [RGBgen, world_P] = view_syn_qry(RGB, XYZ, Kin, hh, hw, Rt)

    x_shift = 8.0*rand(1,1)-4; %[-1.0 1.0]
    y_shift = 8.0*rand(1,1)-4; %[-1.0 1.0]
    z_shift = 2.0*rand(1,1)-1.0; %[-1.0 1.0]
    x_rot = randi([0 40], 1, 1) - 20; % [-20 .. 20]
    y_rot = randi([0 20], 1, 1) - 10; % [-10 .. 10]
    z_rot = randi([0 359], 1, 1);     % [0 .. 359]

    % establish projection matrix in local coordinates
    P_local(1:3, 1:3) = rotx(90-x_rot) * roty(y_rot) * rotz(z_rot);
    P_local(:, 4) = [x_shift y_shift z_shift];

    % transform projection matrix to world coordinates
    world_P = P_local*inv(Rt);

    [RGBgen, ~] = ht_Points2Persp(RGB, XYZ, Kin*world_P, hh, hw); 

end
