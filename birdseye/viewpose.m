function viewpose(pcl_fn, Q)

    % load combined point cloud
    load(pcl_fn);

    % provide desired query image size
    hw = 1600;
    hh = 1200;

    % set camera intrinsics
    f = 10*886*1.57;                        % focal length given insetup
    K = [f, 0, hw/2; 0, f, hh/2; 0, 0, 1];  % set camera intr. matrix

    [~, last_point] = size(XYZ);

    % the pose is provided with the Q parameter as an Rt matric
    Q4 = eye(4);
    Q4(1:3,1:4) = Q;
    invQ4 = inv(Q4);

    % these lines add a marker to visualize the pose on the blueprint
    npoints = 10000;
    randpts = rand(3, npoints) - 0.5;
    randpts(3,1:5000) = (randpts(3,1:5000) .* 4) + 2.5; % shift marker to point towards view direction
    randpts(1,1:5000) = randpts(1,1:5000) .* 0.5; % make marker pointer thinner
    randpts(:,5001:10000) = (randpts(:,5001:10000) .* 2);
    randpts(2,:) = randpts(2,:) - 10; % elevate marker above ground level
    m_XYZ = randpts;
    m_XYZ = invQ4 * [m_XYZ; ones(1, size(m_XYZ, 2))];
    m_XYZ = bsxfun(@rdivide, m_XYZ(1:3, :), m_XYZ(4,:));
    XYZ(:, last_point+1:last_point+npoints) = m_XYZ;
    RGB(:, last_point+1:last_point+5000) = repmat([255; 0; 0], 1, 5000);
    RGB(:, last_point+5001:last_point+npoints) = repmat([0; 0; 255], 1, 5000);

    [view, x, y] = view_syn(RGB, XYZ, K, hh, hw, [0, 45, 1500, -90, 0, 86]);

    imgName = 'output/birdseye.png';
    fprintf(' writing image %s\n', imgName);
    imwrite(view, imgName);
    fprintf('done\n');

end

