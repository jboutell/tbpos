This repository contains the code related to the scientific publication

_TBPos: Dataset for Large-Scale Precision Visual Localization_\
Masud An-Nur Islam Fahim, Luca Ferranti, Ilona Söchting, Juho Kannala, Jani Boutellier\
Scandinavian Conference on Image Analysis (SCIA) 2023

Main contents
-------------

* The Matlab source code used to generate the TBPos database and query images
* An almost complete InLoc algorithm (Taira et al. 2018) package for evaluation
* The set of query images related to the SCIA 2023 paper
* Links to download the full dataset

Contents / source code:
* birdseye --  A script for creating a floorplan view
* common --  Shared files related to missing pixel interpolation
* gen_cutouts -- Script for generating database cutout files
* get_trans -- Script for generating database alignments files
* objextr -- Auxiliary script for extracting object bitmaps
* synth\_from_gt -- Script for generating query images given GT poses
* synth_poses -- Script for generating query image GT poses
* utils -- Small scripts for genering various list files needed by InLoc

Contents / other:
* query -- Query images
* inloc -- InLoc algorithm package

Installing the TBPos dataset
----------------------------

Four software components are required for testing
1) TBPos point cloud scans and scan alignment files
2) TBPos RGBD cutouts
3) TBPos query images
4) The InLoc algorithm and all its dependencies (if you want to test with InLoc)

Below, instructions on acquiring each of these are explained

1. TBPos point cloud scans and alignment files\
The point cloud scans (~100 GB) and associated alignment files are available at https://zenodo.org/record/7466448

2. TBPos RGBD cutouts\
The RGBD cutouts (~100 GB) can either be generated from the point cloud scans using the software provided in the Zenodo repository, or for convenience, be downloaded from fairdata.fi (link: https://etsin.fairdata.fi/dataset/6afbcc97-f513-4434-9e7d-923107cd1b82)

3. TBPos query images\
The set of 338 query images related to the SCIA paper are available in this repository

4. The Inloc algorithm\
InLoc (Taira et al. 2018) runs on Matlab and depends on a number of libraries. Since compiling the InLoc dependencies for recent GPUs can be very difficult, this repository contains a full package, including binaries / mex files, of InLoc for Ubuntu 20.04. No guarantee is provided that it will run on a specific system without modifications, though. Note: the rights to all software provided in the package belongs to their original authors.\
Exception: the Yael v4.38 library is not included, please consult detailed documentation below where to obtain.\
To test InLoc positioning for the TBPos dataset, either download the inloc/InLoc-for-TBPos.tar.gz package from this repository, or refer to the detailed instructions on how to create a working InLoc setup from scratch (inloc/inloc-from-scratch-notes.txt).


After fetching all the above four software components, e.g., the following folder structure can be established:

./inloc/  (from InLoc-for-TBPos.tar.gz, provides:)\
./inloc/ceres-solver-1.14.0/\
./inloc/gflags/\
./inloc/glog-0.4.0/\
./inloc/InLoc_demo/\
./TBPos/ (from query image tarball, provides:)\
./TBPos/query/\
./TBPos/query/scia/\
./TBPos/query/query\_imgnames_all.mat\
./TBPos/database/  (from Zenodo, provides:)\
./TBPos/database/alignments/\
./TBPos/database/scans/\
./TBPos/database/cutout\_imgnames_all.mat\
./TBPos/database/cutouts/ (from fairdata.fi)

Additional steps:
* Download the netvlad CNN vd16\_pitts30k\_conv5\_3\_vlad\_preL2\_intra\_white.mat and place it to InLoc_demo/functions/netvlad/
* Obtain Yael Matlab v4.38, place it in InLoc\_demo/functions/yael\_matlab\_linux64\_v438 and compile it
* Copy query\_imgnames\_all.mat and cutout\_imgnames\_all.mat (see folder structure above) to InLoc_demo/inputs
* A few source code lines need to be changed to adapt InLoc to the TBPos dataset. These changes are listed in required-code-changes.txt

Testing InLoc localization on TBPos dataset
-------------------------------------------

1. Generate scores.mat for image retrieval score calculation by running the following Matlab scripts
  InLoc_demo/buildScores/buildFeatures.m (e.g., matlab -r buildFeatures -nodisplay -nojvm)
  InLoc_demo/buildScores/buildScores.m
  InLoc_demo/buildScores/transformScores.m
2. Start the InLoc algorithm from command line by 
  matlab -r startup -nodisplay -nojvm
  InLoc places the final positioning results to InLoc\_demo/outputs/densePV\_top10\_shortlist.mat

Benchmarking localization accuracy
----------------------------------

The TBPos ground truth is not disclosed. If you would like to test the accuracy of your own localization pipeline on the TBPos dataset, you can send your densePV\_top10\_shortlist.mat (or equivalent) file to tbpos_dot_bm_at_gmail_dot_com and we'll send you back the results. We'll also maintain a benchmark list here out of the submitted results, unless you mention that you would like to withhold the results from publishing.

Alternatively, if you use the software in this repository to create a set of query images yourself, you can use the script in InLoc\_demo/evaluate/success\_rate\_TBPos.m with parameters reference\_poses.mat and densePV\_top10\_shortlist.mat to check how your algorithm performs.
  
|Method	| (0.25m, 10°) / (0.5m, 10°) / (1m, 10°) | Last updated
|-------|----------------------------------------|-------------
|InLoc  | 29.6 / 34.3 / 35.2                     | Dec. 22, 2022, 12:50:52 GMT

Citing our work
---------------

Please cite the following publication:

TBPos: Dataset for Large-Scale Precision Visual Localization
Masud An-Nur Islam Fahim, Luca Ferranti, Ilona Söchting, Juho Kannala, Jani Boutellier
Scandinavian Conference on Image Analysis (SCIA) 2023

The paper and the supplementary material are available in the doc/ folder.

