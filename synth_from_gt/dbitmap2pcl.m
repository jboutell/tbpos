function [XYZ_obj, RGB_obj] = dbitmap2pcl(image, distance, scale, y_offset, alpha, origin, rotation)

    xyz = project_xyz(image, distance, scale, y_offset, alpha);
    XYZ_obj = im2st(xyz)';

    P = [rotx(rotation(1)-90)*roty(rotation(2))*rotz(rotation(3)), origin'];
    P(4, 1:4) = [0 0 0 1];

    XYZ_obj = [XYZ_obj; ones(1, length(XYZ_obj))];
    XYZ_obj = P * XYZ_obj;
    XYZ_obj = bsxfun(@rdivide, XYZ_obj(1:3, :), XYZ_obj(4, :));
    
    RGB_obj = uint8(im2st(image))';
end

