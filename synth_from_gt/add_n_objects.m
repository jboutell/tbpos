function [xyz, rgb] = add_n_objects(origin, direction, count)

    xyz = [];
    rgb = [];

    for i = 0:count-1
        angle = 60*(rand(1,1)-0.5)+direction;
        [xyz, rgb] = add_object(xyz, rgb, angle, origin, i);
    end

end

