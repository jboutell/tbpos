function flythrough(pcl_fn)

    % load combined point cloud (by combine_scans)
    load(pcl_fn);

    %% provide desired frame size
    distance = 10;
    hw = 1024;
    hh = 768;

    %% set camera intrinsics
    f = 886;                                % focal length given insetup
    K = [f, 0, hw/2; 0, f, hh/2; 0, 0, 1];  % set camera intr. matrix

    for i = -50:1:38
        [view, x, y] = view_syn(RGB, XYZ, K, hh, hw, [-21, 0, distance-2*i, 0, 0, -5]);
        imgName = sprintf('output/frame_%i.png', i+50);
        fprintf(' writing image %s\n', imgName);
        imwrite(view, imgName);
    end

    fprintf('done\n');
end


