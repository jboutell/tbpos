function [s1, s2] = view_syn_dtbs(RGB, XYZ, theta, pitch, h, w)

    Rt(1:3,1:3) = rotx(pitch) * rotz(theta);
    Rt(:,4) = [0 0 0];

    % Get focal length that equals to 60 field of view horizontally
    fl = floor((w/2)/tan(pi/180*30));
    Kin = [fl, 0, w/2; 0, fl, h/2; 0, 0, 1];  

    [s1, s2] = ht_Points2Persp(RGB, XYZ, Kin*Rt, h, w);         
end

