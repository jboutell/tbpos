%%% This code provides the database coordinate transformations
for ik = 0:181
    f2d = ['../scans/TB_scan_',sprintf('%03d',ik),'.ptx.mat'];
    T = load(f2d).T;
    write_trans(['../trans_new/TB_trans_',sprintf('%03d',ik),'.txt'], T);
end

